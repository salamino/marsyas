let textString ="MarsyasemergEnce";

let letter = [];
let coords = [];

let xOffset = 15;
let yOffset = 5;

var fontsize;
var canvas;

let margin = 50;

function stick(){
  stickTo = document.getElementById("emergence");
  if(window.scrollY>0){
    stickTo.style.top = document.getElementById('menu').offsetHeight + "px";
    } else {
    stickTo.style.top = stickTo.offsetTop + 'px';
    }
  }

function currentHeight(){
  return document.getElementById("emergence").clientHeight - document.getElementById("menu").clientHeight;
  }

function setup(){
  canvas = createCanvas(document.getElementById("emergence").getBoundingClientRect().width,currentHeight());
  canvas.parent('emergence');
  stick();
  background(10);
  letter = split(textString,'');
  fontSizing();
  randomCover();
  }

function windowResized() {
  var rect = document.getElementById("emergence").getBoundingClientRect();
  var elem = document.getElementById("half");
  resizeCanvas(elem.clientWidth-1, rect.height);
  fontSizing();
  randomCover();
  }

  function fontSizing(){
    var el = document.getElementById('half');
    var style = window.getComputedStyle(el, null).getPropertyValue('font-size');
    fontsize = parseFloat(style); 
  }

function randomCover(){
coords = [];
beginShape();
letter.forEach(function(char, index){
  curveTightness(random(-2,2));
  var x = Math.floor(random(margin/2, canvas.width-margin/2));
  var y = Math.floor(random(margin/2, canvas.height-margin/2));
  
  fill(241, 179, 8);
  noStroke();
  textFont('AnaktoriaRegular',fontsize);
  text(char,x, y);

  stroke(241, 179, 8);
  strokeWeight(1.5*fontsize/20);

  ellipse(x+xOffset,y+yOffset,3,3);
  
  var coord = createVector(x,y);
  coords.push(coord);
  
  if(index !=0 && index != coords.length && index%4==0){
    bezierVertex( coords[index-2].x+xOffset,coords[index-2].y+yOffset,
                  coords[index-1].x+xOffset,coords[index-1].y+yOffset,
                  coords[index].x+xOffset,coords[index].y+yOffset);
      } else if (index==0){
        vertex(coords[index].x+xOffset,coords[index].y+yOffset);
      } else if (index==coords.length){
        vertex(coords[index].x+xOffset,coords[index].y+yOffset);
      }
  })
  noFill();
  endShape();
}



